import { useState } from "react";
import BlogList from "./BlogList";
import { useFetch } from "./useFetch";

const Home = () => {
  const [name, setName] = useState("mario");

  const {
    data: blogs,
    isPending,
    error,
  } = useFetch("http://localhost:8000/blogs");

  // let handleDelete = (id) => {
  //   setBlogs((blogsLocal) => {
  //     return blogsLocal.filter((blog) => {
  //       return blog.id !== id;
  //     });
  //   });
  // };

  // useEffect(() => {
  //   fetch("http://localhost:8000/blogs")
  //     .then((res) => {
  //       console.log(res);
  //       if (!res.ok) {
  //         throw Error("could not fetch data for that resource");
  //       }
  //       return res.json();
  //     })
  //     .then((data) => {
  //       console.log(data);
  //       setBlogs(data);
  //       setIsPending(false);
  //       setError(false);
  //     })
  //     .catch((error) => {
  //       setError(error.message);
  //       setIsPending(false);
  //     });
  // }, []);

  return (
    <div className="home">
      {error && <div>{error}</div>}
      {isPending && <div>Loading...</div>}
      <div>
        {blogs && (
          <BlogList
            blogs={blogs}
            title="All Blogs"
            //handleDelete={handleDelete}
          />
        )}
        {blogs && (
          <BlogList
            blogs={blogs.filter((blog) => blog.author === "mario")}
            title="Mario's blogs"
            //handleDelete={handleDelete}
          />
        )}
        <button onClick={() => setName("Luigi")}>change name</button>
        <p>{name}</p>
      </div>
    </div>
  );
};

export default Home;
