import { useState, useEffect } from "react";

export const useFetch = (url) => {
  const [data, setData] = useState(null);
  const [isPending, setIsPending] = useState(true);
  const [error, setError] = useState(false);

  useEffect(() => {
    const abortConst = new AbortController();
    fetch(url, { signal: abortConst.signal })
      .then((res) => {
        //console.log(res);
        if (!res.ok) {
          throw Error("could not fetch data for that resource");
        }
        return res.json();
      })
      .then((dataLocal) => {
        //console.log(dataLocal);
        setData(dataLocal);
        setIsPending(false);
        setError(false);
      })
      .catch((error) => {
        if (error.name === "AbortError") {
          console.log("fetch aborted");
        } else {
          setError(error.message);
          setIsPending(false);
        }
      });
    return () => abortConst.abort();
  }, [url]);
  return { data, isPending, error };
};
